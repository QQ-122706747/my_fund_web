// 获得当前时间 格式 yyyy-dd-mm
export function getCurrentTime() {
  const date = new Date()
  const y = date.getFullYear()
  let m = date.getMonth() + 1
  m = m < 10 ? ('0' + m) : m
  let d = date.getDate()
  d = d < 10 ? ('0' + d) : d
  const time = y + '-' + m + '-' + d
  return time
}
// 获得本周的开始日期
export function getWeekStartDate() {
  const now = new Date()
  return new Date(now.getTime() + ((-now.getDay() + 1) * (1000 * 60 * 60 * 24)))
}
// 获得本周的结束日期
export function getWeekEndDate() {
  return new Date(getWeekStartDate().getTime() + 3600 * 1000 * 24 * 6)
}
// 获得本月的开始日期
export function getMonthStartDate() {
  const now = new Date()
  return new Date(now.getFullYear(), now.getMonth(), 1)
}
// 获得本月的结束日期
export function getMonthEndDate() {
  const now = new Date()
  return new Date(now.getFullYear(), now.getMonth() + 1, 0)
}
// 获得本季度的开始日期
export function getQuarterStartDate() {
  const now = new Date()
  return new Date(now.getFullYear(), getQuarterStartMonth(), 1)
}
// 或的本季度的结束日期
export function getQuarterEndDate() {
  const now = new Date()
  const quarterEndMonth = getQuarterStartMonth() + 3
  return new Date(now.getFullYear(), quarterEndMonth, getMonthDays(quarterEndMonth))
}
// 获得某月的天数
export function getMonthDays(myMonth) {
  const nowYear = new Date().getFullYear()
  const monthStartDate = new Date(nowYear, myMonth, 1)
  const monthEndDate = new Date(nowYear, myMonth + 1, 1)
  const days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24)
  return days
}
// 获得本季度的开始月份
export function getQuarterStartMonth() {
  const nowMonth = new Date().getMonth()
  let quarterStartMonth = 0
  if (nowMonth < 3) {
    quarterStartMonth = 0
  }
  if (nowMonth > 2 && nowMonth < 6) {
    quarterStartMonth = 2
  }
  if (nowMonth > 5 && nowMonth < 9) {
    quarterStartMonth = 5
  }
  if (nowMonth > 8) {
    quarterStartMonth = 8
  }
  return quarterStartMonth
}
