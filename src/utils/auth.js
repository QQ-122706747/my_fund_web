// import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

export function getToken() {
  // return Cookies.get(TokenKey)
  return sessionStorage.getItem(TokenKey)
}

export function setToken(token) {
  // return Cookies.set(TokenKey, token)
  sessionStorage.setItem(TokenKey, token)
  // 一次性清除 setTimeout()
  // 循环清除 可用于刷新 setInterval()
  // 30分钟后清除
  // setTimeout(function() {
  //   sessionStorage.removeItem(TokenKey)
  // }, 30 * 60 * 1000)
}

export function removeToken() {
  // return Cookies.remove(TokenKey)
  // return sessionStorage.removeItem(TokenKey)

  // 清除所有
  return sessionStorage.clear()
}
