/** jwt 配置管理**/

import Layout from '@/layout'

const jwtAdminRouter = {
  path: '/jwt-admin',
  component: Layout,
  redirect: '/jwt-admin/tenant',
  name: '认证配置',
  meta: {
    title: '认证配置',
    icon: 'table'
  },
  children: [
    {
      path: 'tenant',
      component: () => import('@/views/jwt-admin/tenant'),
      name: '租户管理',
      meta: { title: '租户管理' }
    },
    {
      path: 'group',
      component: () => import('@/views/jwt-admin/group'),
      name: '租户组管理',
      meta: { title: '租户组管理' }
    }
  ]
}
export default jwtAdminRouter
