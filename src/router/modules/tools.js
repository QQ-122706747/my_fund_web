
import Layout from '@/layout'

const toolsRouter = {
  path: '/tools',
  component: Layout,
  redirect: '',
  name: '工具管理',
  meta: {
    title: '基金管理',
    icon: 'table'
  },
  children: [
    {
      path: 'fund',
      component: () => import('@/views/tools/fund'),
      name: '基金管理',
      meta: { title: '基金管理' }
    },
    {
      path: 'fund/report',
      component: () => import('@/views/tools/fund/report'),
      name: '报告管理',
      meta: { title: '报告管理' }
    },
    {
      hidden: true,
      path: 'fund/reportDetail',
      component: () => import('@/views/tools/fund/reportDetail'),
      name: '详细报告',
      meta: { title: '详细报告' }
    },
    {
      path: 'fund/hc',
      component: () => import('@/views/tools/fund/huiche'),
      name: '回撤查询',
      meta: { title: '回撤查询' }
    },
    {
      path: 'fund/zj',
      component: () => import('@/views/tools/fund/zj'),
      name: '历史净值查询',
      meta: { title: '历史净值管理' }
    },
    {
      path: 'fund/cal',
      component: () => import('@/views/tools/fund/cal'),
      name: '基金定投收益计算器',
      meta: { title: '基金定投查询' }
    },
    {
      path: 'fund/group',
      component: () => import('@/views/tools/fund/group'),
      name: '组合管理',
      meta: { title: '组合管理' }
    },
    {
      path: 'fund/item',
      component: () => import('@/views/tools/fund/item'),
      name: '交易记录',
      meta: { title: '交易记录' }
    },
    {
      hidden: true,
      path: 'fund/rateOfReturn',
      component: () => import('@/views/tools/fund/rateOfReturn'),
      name: '统计收益率',
      meta: { title: '统计收益率' }
    },
    {
      hidden: true,
      path: 'fund/creatJZ',
      component: () => import('@/views/tools/fund/creatJZ'),
      name: '生成净值数据',
      meta: { title: '生成净值数据' }
    },
    {
      hidden: true,
      path: 'fund/creatGroup',
      component: () => import('@/views/tools/fund/creatGroup'),
      name: '新增组合',
      meta: { title: '新增组合' }
    },
    {
      hidden: true,
      path: 'fund/updateGroup',
      component: () => import('@/views/tools/fund/updateGroup'),
      name: '更新组合',
      meta: { title: '更新组合' }
    },
    {
      hidden: true,
      path: 'fund/reportGroup',
      component: () => import('@/views/tools/fund/reportGroup'),
      name: '简单的组报告',
      meta: { title: '简单的组报告' }
    },
    {
      hidden: true,
      path: 'fund/creatFund',
      component: () => import('@/views/tools/fund/creatFund'),
      name: '新增基金',
      meta: { title: '新增基金' }
    },
    {
      hidden: true,
      path: 'fund/updateFund',
      component: () => import('@/views/tools/fund/updateFund'),
      name: '编辑基金',
      meta: { title: '编辑基金' }
    },
    {
      // hidden: true,
      path: 'fund/autoItem',
      component: () => import('@/views/tools/fund/autoItem'),
      name: '定投设置',
      meta: { title: '定投设置' }
    },
    {
      hidden: true,
      path: 'fund/group/bingTu',
      component: () => import('@/views/tools/fund/bingTu'),
      name: '交易记录饼图',
      meta: { title: '交易记录饼图' }
    },
    {
      hidden: true,
      path: 'fund/group/zheXianTu',
      component: () => import('@/views/tools/fund/zheXianTu'),
      name: '折线图',
      meta: { title: '折线图' }
    }
  ]
}
export default toolsRouter
