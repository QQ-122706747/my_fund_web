import Layout from '@/layout'

const adminRouter = {
  path: '/admin',
  component: Layout,
  name: '系统管理2',
  meta: {
    title: '系统管理2',
    icon: 'table'
  },
  children: [
    {
      path: 'tenant2',
      component: () => import('@/views/admin/tenant'),
      name: '租户管理2',
      meta: { title: '租户管理2' }
    },
    {
      path: 'tenant3',
      component: () => import('@/views/admin/tenant'),
      name: '接入系统管理',
      meta: { title: '接入系统管理' }
    }
  ]
}
export default adminRouter
