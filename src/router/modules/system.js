/** system 系统管理**/

import Layout from '@/layout'

const systemRouter = {
  path: '/system',
  component: Layout,
  name: '系统管理',
  meta: {
    title: '系统管理',
    icon: 'table'
  },
  children: [
    {
      path: 'user',
      component: () => import('@/views/system/user'),
      name: '用户管理',
      meta: { title: '用户管理' }
    },
    {
      path: 'group',
      component: () => import('@/views/system/group'),
      name: '用户组管理',
      meta: { title: '用户组管理' }
    },
    {
      path: 'role',
      component: () => import('@/views/system/role'),
      name: '角色管理',
      meta: { title: '角色管理' }
    },
    {
      path: 'menu',
      component: () => import('@/views/system/menu'),
      name: '菜单管理',
      meta: { title: '菜单管理' }
    }
  ]
}
export default systemRouter
