import request from '@/utils/request'

export function getListTreeApi(data) {
  return request({
    url: 'admin/api/group/list/tree',
    method: 'post',
    data
  })
}

export function addApi(data) {
  return request({
    url: '/admin/api/group/add',
    method: 'post',
    data
  })
}

export function getDetailApi(data) {
  return request({
    url: '/admin/api/group/get',
    method: 'post',
    data
  })
}

export function updateApi(data) {
  return request({
    url: '/admin/api/group/update',
    method: 'post',
    data
  })
}

export function deleteByCodeApi(data) {
  return request({
    url: '/admin/api/group/delete',
    method: 'post',
    data
  })
}

export function getUserRelationPageListByGroupApi(data) {
  return request({
    url: 'admin/api/group/user/getPage',
    method: 'post',
    data
  })
}

export function getUsersNotSetPageListByGroupApi(data) {
  return request({
    url: 'admin/api/group/userNotSetPage',
    method: 'post',
    data
  })
}

export function addGroupUserApi(data) {
  return request({
    url: '/admin/api/group/add/user',
    method: 'post',
    data
  })
}

export function deleteGroupAndUserRelationApi(data) {
  return request({
    url: '/admin/api/group/delete/user',
    method: 'post',
    data
  })
}
