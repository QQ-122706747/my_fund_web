import request from '@/utils/request'

export function getPageApi(data) {
  return request({
    url: 'admin/api/user/getPage',
    method: 'post',
    data
  })
}

export function addApi(data) {
  return request({
    url: '/admin/api/user/add',
    method: 'post',
    data
  })
}

export function updateApi(data) {
  return request({
    url: '/admin/api/user/update',
    method: 'post',
    data
  })
}

export function deleteByCodeApi(data) {
  return request({
    url: '/admin/api/user/delete',
    method: 'post',
    data
  })
}

export function importExcelApi(data) {
  return request({
    url: '/admin/api/user/importExcel',
    method: 'post',
    data
  })
}
