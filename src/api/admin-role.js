import request from '@/utils/request'

export function getListTreeApi(data) {
  return request({
    url: 'admin/api/role/list/tree',
    method: 'post',
    data
  })
}

export function getDetailApi(data) {
  return request({
    url: 'admin/api/role/get',
    method: 'post',
    data
  })
}

export function updateApi(data) {
  return request({
    url: '/admin/api/role/update',
    method: 'post',
    data
  })
}

export function addApi(data) {
  return request({
    url: '/admin/api/role/add',
    method: 'post',
    data
  })
}

export function deleteByCodeApi(data) {
  return request({
    url: '/admin/api/role/delete',
    method: 'post',
    data
  })
}

export function getPageApi(data) {
  return request({
    url: 'admin/api/role/getPage',
    method: 'post',
    data
  })
}

export function addRoleUserApi(data) {
  return request({
    url: '/admin/api/role/add/user',
    method: 'post',
    data
  })
}

export function deleteRoleAndUserRelationApi(data) {
  return request({
    url: '/admin/api/role/delete/user',
    method: 'post',
    data
  })
}

export function getUserRelationPageListByRoleApi(data) {
  return request({
    url: 'admin/api/role/user/getPage',
    method: 'post',
    data
  })
}

export function getUsersNotSetPageListByRoleApi(data) {
  return request({
    url: 'admin/api/role/userNotSetPage',
    method: 'post',
    data
  })
}
export function getGroupRelationPageListByRoleApi(data) {
  return request({
    url: 'admin/api/role/group/getPage',
    method: 'post',
    data
  })
}

export function getGroupsNotSetPageListByRoleApi(data) {
  return request({
    url: 'admin/api/role/groupNotSetPage',
    method: 'post',
    data
  })
}

export function addRoleGroupApi(data) {
  return request({
    url: '/admin/api/role/add/group',
    method: 'post',
    data
  })
}

export function deleteRoleAndGroupRelationApi(data) {
  return request({
    url: '/admin/api/role/delete/group',
    method: 'post',
    data
  })
}
