import request from '@/utils/request'

export function getPageApi(data) {
  return request({
    url: '/admin/sso/group/getPage',
    method: 'post',
    data
  })
}
export function addApi(data) {
  return request({
    url: '/admin/sso/group/add',
    method: 'post',
    data
  })
}

export function updateApi(data) {
  return request({
    url: '/admin/sso/group/update',
    method: 'post',
    data
  })
}

export function deleteByCodeApi(data) {
  return request({
    url: '/admin/sso/group/delete/code',
    method: 'post',
    data
  })
}

export function getGroupList() {
  return request({
    url: '/admin/sso/group/getList',
    method: 'get'
  })
}
