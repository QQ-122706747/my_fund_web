import request from '@/utils/request'

export function getTenantStatusApi() {
  return request({
    url: 'admin/api/tenant/getTenantStatus',
    method: 'get'
  })
}
export function tenantGetSysListApi(data) {
  return request({
    url: 'admin/api/tenant/getSysList',
    method: 'post',
    data
  })
}

export function addTenantApi(data) {
  return request({
    url: 'admin/api/tenant/add',
    method: 'post',
    data
  })
}

export function updateTenantApi(data) {
  return request({
    url: 'admin/api/tenant/update',
    method: 'post',
    data
  })
}

export function getTenantListApi(data) {
  return request({
    url: 'admin/api/tenant/getList',
    method: 'post',
    data
  })
}
export function deleteTenantByIdApi(data) {
  return request({
    url: 'admin/api/tenant/delete',
    method: 'post',
    data
  })
}
