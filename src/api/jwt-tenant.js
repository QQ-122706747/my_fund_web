import request from '@/utils/request'

export function getPageApi(data) {
  return request({
    url: '/admin/sso/tenant/getPage',
    method: 'post',
    data
  })
}
export function addApi(data) {
  return request({
    url: '/admin/sso/tenant/add',
    method: 'post',
    data
  })
}

export function updateApi(data) {
  return request({
    url: '/admin/sso/tenant/update',
    method: 'post',
    data
  })
}

export function deleteByCodeApi(data) {
  return request({
    url: '/admin/sso/tenant/delete/code',
    method: 'post',
    data
  })
}

