import request from '@/utils/request'
// 获取验证码
export function getLoginCodeApi() {
  return request({
    url: '/admin/api/login/code',
    method: 'get'
  })
}
// 登录
export function loginApi(data) {
  return request({
    url: '/fund/api/login',
    method: 'post',
    data
  })
}
// 登录
export function loginOutApi() {
  return request({
    url: '/fund/api/login/out',
    method: 'get'
  })
}

export function getUserInfoByLoginApi() {
  return request({
    url: '/fund/api/login/userInfo',
    method: 'get'
  })
}

export function getNewLoginApi(data) {
  return request({
    url: '/admin/api/login/getUrl',
    method: 'post',
    data
  })
}
export function getNewLoginApi2() {
  return request({
    url: '/admin/api/login/getUrl2',
    method: 'post'
  })
}
