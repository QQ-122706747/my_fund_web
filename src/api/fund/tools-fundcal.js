import request from '@/utils/request'

export function getPageApi(data) {
  return request({
    url: '/fund/api/regular/getPage',
    method: 'post',
    data
  })
}
export function getFundNameApi(data) {
  return request({
    url: '/fund/api/regular/getFundName',
    method: 'post',
    data
  })
}
export function getDateListApi() {
  return request({
    url: '/fund/api/regular/date/list',
    method: 'get'
  })
}
export function addApi(data) {
  return request({
    url: '/fund/api/regular/getData',
    method: 'post',
    data
  })
}
//
// export function updateApi(data) {
//   return request({
//     url: '/admin/sso/group/update',
//     method: 'post',
//     data
//   })
// }
//
// export function deleteByCodeApi(data) {
//   return request({
//     url: '/admin/sso/group/delete/code',
//     method: 'post',
//     data
//   })
// }
//
// export function getGroupList() {
//   return request({
//     url: '/admin/sso/group/getList',
//     method: 'get'
//   })
// }
