import request from '@/utils/request'

export function getPageApi(data) {
  return request({
    url: '/fund/api/item/getPage',
    method: 'post',
    data
  })
}

export function getRateOfReturnInfoApi(data) {
  return request({
    url: '/fund/api/item/getRateInfo',
    method: 'post',
    data
  })
}

export function addItemApi(data) {
  return request({
    url: '/fund/api/item/add',
    method: 'post',
    data
  })
}

export function deleteItemApi(data) {
  return request({
    url: '/fund/api/item/delete',
    method: 'post',
    data
  })
}
export function plDeleteItemApi(data) {
  return request({
    url: '/fund/api/item/pl/delete',
    method: 'post',
    data
  })
}

export function getPieDataApi(data) {
  return request({
    url: '/fund/api/item/getPieData',
    method: 'post',
    data
  })
}
