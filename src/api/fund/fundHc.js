import request from '@/utils/request'

export function getPageApi(data) {
  return request({
    url: '/fund/api/hc/getPage',
    method: 'post',
    data
  })
}
