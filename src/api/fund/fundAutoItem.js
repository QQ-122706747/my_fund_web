import request from '@/utils/request'

export function addItemApi(data) {
  return request({
    url: '/fund/api/auto/item/addItem',
    method: 'post',
    data
  })
}
export function getAutoItemPageApi(data) {
  return request({
    url: '/fund/api/auto/item/getPage',
    method: 'post',
    data
  })
}

export function addAutoItemApi(data) {
  return request({
    url: '/fund/api/auto/item/add',
    method: 'post',
    data
  })
}

export function deleteAutoItemApi(data) {
  return request({
    url: '/fund/api/auto/item/delete',
    method: 'post',
    data
  })
}
