import request from '@/utils/request'

export function getPageApi(data) {
  return request({
    url: '/fund/api/group/getPage',
    method: 'post',
    data
  })
}
export function getFundGroupListApi(data) {
  return request({
    url: '/fund/api/group/getFundGroupList',
    method: 'post',
    data
  })
}
export function getFundGroupSelectListApi(data) {
  return request({
    url: '/fund/api/group/getFundGroupSelectList',
    method: 'post',
    data
  })
}
export function addApi(data) {
  return request({
    url: '/fund/api/group/add',
    method: 'post',
    data
  })
}

export function addFundGroupListApi(data) {
  return request({
    url: '/fund/api/groups/add',
    method: 'post',
    data
  })
}

export function updateApi(data) {
  return request({
    url: '/fund/api/group/update',
    method: 'post',
    data
  })
}

export function deleteByCodeApi(data) {
  return request({
    url: '/fund/api/group/delete',
    method: 'post',
    data
  })
}

export function getGroupSelectListApi() {
  return request({
    url: '/fund/api/group/getSelectList',
    method: 'get'
  })
}
export function getFundListByIdApi(data) {
  return request({
    url: '/fund/api/group/getFundList',
    method: 'post',
    data
  })
}

export function itemDownloadApi(data) {
  return request({
    url: '/fund/api/item/download',
    method: 'post',
    data
  })
}
