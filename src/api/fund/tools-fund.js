import request from '@/utils/request'

export function creatReportApi(data) {
  return request({
    url: '/fund/api/report/create',
    method: 'post',
    data
  })
}
export function getPageApi(data) {
  return request({
    url: '/fund/api/getPage',
    method: 'post',
    data
  })
}
export function addApi(data) {
  return request({
    url: '/fund/api/add',
    method: 'post',
    data
  })
}

export function getAllFundListApi(data) {
  return request({
    url: '/fund/api/getList',
    method: 'post',
    data
  })
}

export function getBaseInfoApi(data) {
  return request({
    url: '/fund/api/getInfo',
    method: 'post',
    data
  })
}
export function getBaseInfoForUpdateApi(data) {
  return request({
    url: '/fund/api/update/getInfo',
    method: 'post',
    data
  })
}
export function getFundTypeApi() {
  return request({
    url: '/fund/api/getFundType',
    method: 'get'
  })
}
export function getSubjectSelectListApi() {
  return request({
    url: '/fund/api/getSubjectList',
    method: 'get'
  })
}

export function updateApi(data) {
  return request({
    url: '/fund/api/update',
    method: 'post',
    data
  })
}

export function deleteByCodeApi(data) {
  return request({
    url: '/fund/api/delete',
    method: 'post',
    data
  })
}
export function getJZDataApi(data) {
  return request({
    url: '/fund/api/jz/getData',
    method: 'post',
    data
  })
}
