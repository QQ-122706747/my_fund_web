import request from '@/utils/request'

export function getZxzjApi(data) {
  return request({
    url: '/fund/api/jz/now',
    method: 'post',
    data
  })
}
export function getJZDataApi(data) {
  return request({
    url: '/fund/api/jz/getData',
    method: 'post',
    data
  })
}

export function getPageApi(data) {
  return request({
    url: '/fund/api/jz/getPage',
    method: 'post',
    data
  })
}
export function getYearsByCodeApi(data) {
  return request({
    url: '/fund/api/jz/getYearsByCode/' + data,
    method: 'get'
  })
}
export function getLineDataApi(data) {
  return request({
    url: '/fund/api/jz/getLineData',
    method: 'post',
    data
  })
}
