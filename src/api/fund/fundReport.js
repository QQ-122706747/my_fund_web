import request from '@/utils/request'

export function deleteApi(data) {
  return request({
    url: '/fund/api/report/delete',
    method: 'post',
    data
  })
}
export function getDownPageApi(data) {
  return request({
    url: '/fund/api/report/getDownPage',
    method: 'post',
    data
  })
}
export function getPageApi(data) {
  return request({
    url: '/fund/api/report/getPage',
    method: 'post',
    data
  })
}
export function updateReportApi(data) {
  return request({
    url: '/fund/api/report/update',
    method: 'post',
    data
  })
}

export function updateAllReportApi() {
  return request({
    url: '/fund/api/report/all/update',
    method: 'get'
  })
}
export function searchReportApi(data) {
  return request({
    url: '/fund/api/report/search',
    method: 'post',
    data
  })
}

export function reCreateReportApi(data) {
  return request({
    url: '/fund/api/report/re/create',
    method: 'post',
    data
  })
}
export function getReportInfoApi(data) {
  return request({
    url: '/fund/api/report/getInfo',
    method: 'post',
    data
  })
}
export function getHCInfoByCodeApi(data) {
  return request({
    url: '/fund/api/hc/getPage',
    method: 'post',
    data
  })
}
export function getDTInfoByCodeApi(data) {
  return request({
    url: '/fund/api/regular/getPage',
    method: 'post',
    data
  })
}
export function groupDownloadApi(data) {
  return request({
    url: '/fund/api/report/group/download',
    method: 'post',
    data
  })
}
export function downloadAllApi() {
  return request({
    url: '/fund/api/report/all/download',
    method: 'get'
  })
}
export function getReportByGroupInfoApi(data) {
  return request({
    url: '/fund/api/report/group/info',
    method: 'post',
    data
  })
}
